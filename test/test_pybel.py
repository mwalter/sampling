import pytest
openbabel = pytest.importorskip("openbabel")
import numpy as np
from ase.build import molecule
from ase.optimize import BFGS

from sampling.calculators.pybel import Pybel
from sampling.optimize.internal import Internal


def test_energy_forces():
    atoms = molecule('CH2OCH2')

    atoms.calc = Pybel()
    E0 = atoms.get_potential_energy()

    opt = BFGS(atoms, trajectory='relax.traj')
    opt.run(fmax=0.01)
    assert E0 > atoms.get_potential_energy()


def test_relaxtion():
    formula = 'CH2OCH2'
    forcefield = 'uff'

    atoms0 = molecule(formula)
    atoms0.calc = Pybel(forcefield=forcefield)

    fmax = 0.05
    opt = BFGS(atoms0)
    opt.run(fmax=fmax)

    atoms1 = molecule(formula)
    atoms1.calc = Pybel(forcefield=forcefield)

    opt = Internal(atoms1)
    opt.run(fmax=fmax, steps=500)

    assert atoms0.get_potential_energy() == pytest.approx(
        atoms1.get_potential_energy(), 1e-2)


def test_largely_stretched():
    atoms = molecule('H2O')
    atoms.calc = Pybel()
    # this call defines the connectivity
    E0 = atoms.get_potential_energy()

    # relax
    fmax = 0.05
    opt = Internal(atoms)
    opt.run(fmax=fmax)
    E0 = atoms.get_potential_energy()

    def displace(atoms):
        atoms[1].position[0] += 4
        E1 = atoms.get_potential_energy()
        assert E1 == pytest.approx(354890, 1)

    # the connectivity is kept despite of structure change
    atoms[1].position[0] += 4
    E1 = atoms.get_potential_energy()
    assert E1 == pytest.approx(354890, 1)

    # relax again
    opt = Internal(atoms)
    fmax = 0.0005
    opt.run(fmax=fmax)
    E1 = atoms.get_potential_energy()

    assert E1 == pytest.approx(E0, abs=1e-5)
    assert np.abs(atoms.get_forces()).max() < fmax


def test_todict():
    forcefield = 'uff'
    calc = Pybel(forcefield=forcefield)
    assert calc.todict()['forcefield'] == forcefield
