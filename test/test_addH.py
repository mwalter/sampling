import numpy as np
import pytest
from ase import Atoms
from ase.build import molecule

from sampling.bond_order import nbonds, bondlengths
from sampling.hydrogen import (bonded_indices, add_sp1, add_sp2, add_sp3,
                               stripH, addH)


def test_sp3_methane():
    atoms = molecule('CH4')
    rng = np.random.RandomState(42)  # ensure the same seed

    # nothing to add
    atoms = add_sp3(atoms, 0, rng=rng)
    assert len(bonded_indices(atoms, 0)) == 4

    def hperp(atoms) -> bool:
        """are the H atoms perpendicular to each other ?"""
        vec = np.zeros(3)
        for ia, atom in enumerate(atoms):
            if ia:
                vec += atom.position - atoms[0].position
        return np.allclose(vec, 0, atol=0.1)

    # add one
    del atoms[-1]

    assert len(bonded_indices(atoms, 0)) == 3
    atoms = add_sp3(atoms, 0, rng=rng)
    assert len(bonded_indices(atoms, 0)) == 4
    assert hperp(atoms)

    # add two
    del atoms[-1]
    del atoms[-1]
    atoms = add_sp3(atoms, 0, rng=rng)
    assert len(bonded_indices(atoms, 0)) == 4
    assert hperp(atoms)

    # add three
    del atoms[-1]
    del atoms[-1]
    del atoms[-1]
    atoms = add_sp3(atoms, 0, rng=rng)
    assert len(bonded_indices(atoms, 0)) == 4
    assert hperp(atoms)


def test_sp2_formaldehyde():
    # remove one
    atoms = molecule('H2CO')
    del atoms[-1]
    atoms = add_sp2(atoms, 1)
    assert len(atoms) == 4

    # remove two
    del atoms[-1]
    del atoms[-1]
    rng = np.random.RandomState(42)  # ensure the same seed
    atoms = add_sp2(atoms, 1, rng=rng)
    assert len(atoms) == 4


def test_sp1_HCN():
    # remove one
    atoms = molecule('HCN')
    del atoms[-1]
    atoms = add_sp1(atoms, 0)
    assert len(atoms) == 3


def test_sp3_xh():
    """add from one existing hydrogen"""
    rng = np.random.RandomState(42)  # ensure the same seed
    for element in 'CNOS':
        r = bondlengths[element + 'H'][0]
        n = nbonds[element]
        atoms = Atoms(element + 'H', positions=[[0, 0, 0], [r, 0, 0]])
        atoms = add_sp3(atoms, 0, rng=rng)

        assert len(atoms) == n + 1
        for i in range(1, n + 1):  # check all bond lengths
            assert np.linalg.norm(atoms[i].position) == pytest.approx(r)


def test_sp3_x():
    """add without existing hydrogen"""
    rng = np.random.RandomState(42)  # ensure the same seed
    for element in 'CNOS':
        r = bondlengths[element + 'H'][0]
        n = nbonds[element]
        atoms = Atoms(element, positions=[[0, 0, 0]])
        atoms = add_sp3(atoms, 0, rng=rng)

        assert len(atoms) == n + 1
        for i in range(1, n + 1):  # check all bond lengths
            assert np.linalg.norm(atoms[i].position) == pytest.approx(r)


def test_auto_sp2_sp3():
    for name in [
        'H2CO',  # sp2
        'CH3CHO',  # sp2 + sp3
        'CH3CH2OH',  # sp3
        'C6H6',  # sp2
        'C5H5N',  # sp2
        'HCN',  # sp1 XXX
        'C60',
    ]:
        atoms = molecule(name)
        noH = stripH(atoms)
        for atom in noH:
            assert atom.symbol != 'H'
        withH = addH(noH)
        assert len(withH) == len(atoms)
