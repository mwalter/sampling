import pytest
import numpy as np
from ase.build import molecule

from sampling.bond_order import (
    bond_order, bonded_indices, sp, sp2_vector, lone_pair_direction)


def test_bond_order():
    assert int(bond_order(molecule('C2H6'), 0, 1) + 0.5) == 1
    assert int(bond_order(molecule('C2H4'), 0, 1) + 0.5) == 2
    assert int(bond_order(molecule('C2H2'), 0, 1) + 0.5) == 3
    assert int(bond_order(molecule('CO'), 0, 1) + 0.5) == 2
    assert bond_order(molecule('C6H6'), 0, 1) == pytest.approx(1.67, abs=0.01)


def test_sp():
    assert sp(molecule('C2H6'), 0) == 3
    assert sp(molecule('C2H4'), 0) == 2
    assert sp(molecule('C2H2'), 0) == 1
    assert sp(molecule('HCN'), 0) == 1
    assert sp(molecule('HCN'), 1) == 1
    assert sp(molecule('C6H6'), 0) == 2

    assert sp(molecule('H2CO'), 1) == 2

    # no hybridization can be assigned
    assert sp(molecule('HCN'), 2) is None


def test_sp2_vector():
    """Ensure that all bonds are perpendicular to the sp2_vector"""
    atoms = molecule('C2H4')
    pos_ac = atoms.get_positions()

    ia = 0
    h_c = sp2_vector(atoms, ia)
    # ensure unit vector
    assert np.linalg.norm(h_c) == pytest.approx(1)

    # ensure orthogonality of all bonds
    for ib in bonded_indices(atoms, ia):
        assert np.dot(h_c, pos_ac[ia] - pos_ac[ib]) == pytest.approx(0)


def test_lone_pair_vector():
    for name in ['NH3', 'SH2']:
        atoms = molecule(name)
        ia = 0
        h_c = lone_pair_direction(atoms, ia)
        for ib in bonded_indices(atoms, ia):
            # make sure all point into opposite directions
            assert np.dot(h_c, atoms[ib].position - atoms[ia].position) < 0


def test_lone_pair_pyrrole():
    """pyrrole has three flat bonds
    and we want to be perpendicular"""
    atoms = molecule('C4H4NH')
    h_c = lone_pair_direction(atoms, 1)

    # perpendicular direction is x
    assert h_c[1] == pytest.approx(0)
    assert h_c[2] == pytest.approx(0)
