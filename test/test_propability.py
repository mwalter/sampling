import pytest
import numpy as np
from ase.units import kB

from sampling.probability import probabilities, partition_sum, entropy


def test_probability():
    E = 10
    N = 3
    energies = [E] * N
    T_K = 300
    p0 = probabilities(energies, T_K)
    assert np.allclose(p0, 1 / N)

    Z, Eref = partition_sum(energies, T_K)
    assert Z == pytest.approx(3)
    assert Eref == pytest.approx(E)

    rng = np.random.RandomState(42)
    energies = 10 * rng.random(N)
    p0 = probabilities(energies, T_K)
    assert p0.sum() == pytest.approx(1)
    p1 = probabilities(energies, T_K,
                       *partition_sum(energies, T_K))
    assert np.allclose(p0, p1)


def test_entropy():
    probabilities = [0.5] * 2
    assert entropy(probabilities) == pytest.approx(-kB * np.log(0.5))

    assert entropy([0, 1]) == 0
