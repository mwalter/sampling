from ase.build import molecule
from sampling.carbon import coordination, count


def test_methane():
    atoms = molecule('CH4')
    coord = coordination(atoms)
    assert len(coord) == len(atoms)
    # the Carbon is bound to 4 hydrogens
    assert coord[0] == {'H': 4}
    # 4 coordinated C only
    assert (count(atoms)[:5] == [0, 0, 0, 0, 100]).all()


def test_benzene():
    atoms = molecule('C6H6')
    coord = coordination(atoms)
    assert len(coord) == len(atoms)
    # the Carbon is bound to two C and one H
    assert coord[0] == {'C': 2, 'H': 1}
    # 3 coordinated C only
    assert (count(atoms)[:5] == [0, 0, 0, 100, 0]).all()
