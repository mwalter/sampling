import pytest
from ase import Atoms
from ase.optimize import FIRE

openbabel = pytest.importorskip("openbabel")
from sampling.calculators.pybel import Pybel
from sampling.calculators.uff import UFF
from sampling.optimize.internal import Internal, internal_or_fire


def test_switch():
    atoms = Atoms('H')

    # calculator that has optimize
    atoms.calc = Pybel()
    assert type(internal_or_fire(atoms)) is type(Internal(atoms))

    # calculator that has no optimize
    atoms.calc = UFF()
    assert type(internal_or_fire(atoms)) is type(FIRE(atoms))
