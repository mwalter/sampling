import pytest
import numpy as np
from ase import Atoms
from ase.optimize import FIRE

from sampling.calculators.mueser2019 import Mueser2019


def test_dimer():
    dimer = Atoms('H2', positions=((0, 0, 0), (0, 0, 1.1)))
    dimer.calc = Mueser2019()

    assert dimer.get_potential_energy() == pytest.approx(0.1**2 / 2)
    f_ac = dimer.get_forces()
    assert np.abs(f_ac[:, :-1]) == pytest.approx(0)
    assert np.allclose(f_ac[0], - f_ac[1])

    dyn = FIRE(dimer)
    dyn.run(fmax=0.01)
    assert dimer.get_potential_energy() == pytest.approx(0, abs=1e-6)
    assert dimer.get_distance(0, 1) == pytest.approx(1, abs=1e-3)


def test_trimer():
    trimer = Atoms('H3', positions=((1.1, 0, 0), (0, 0, 0), (0, 0, 1.1)))

    trimer.calc = Mueser2019(k2=0)
    assert trimer.get_potential_energy() == pytest.approx(0.1**2)

    trimer.calc = Mueser2019()
    assert trimer.get_potential_energy() == pytest.approx(1.1**2 / 4 + 0.1**2)

    dyn = FIRE(trimer)
    dyn.run(fmax=0.01)
    assert trimer.get_distance(0, 1) == pytest.approx(1, abs=1e-3)
    assert trimer.get_distance(2, 1) == pytest.approx(1, abs=1e-3)
