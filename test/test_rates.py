import pytest

from sampling.rates import Eyring_rate, Eyring_barrier


def test_Eyring():
    T0 = 300
    assert Eyring_rate(0, 2 * T0) == pytest.approx(2 * Eyring_rate(0))


def test_Eyring_barrier():
    T0 = 300
    barrier = 1  # eV
    rate = Eyring_rate(barrier, T_K=T0)

    assert Eyring_barrier(rate, T_K=T0) == pytest.approx(barrier)
