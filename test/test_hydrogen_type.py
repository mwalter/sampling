from ase.build import molecule

from sampling.translate import from_smiles
from sampling.hydrogen import hydrogen_types


def test_assignment_c3h8():
    atoms = molecule('C3H8')
    htypes = hydrogen_types(atoms)

    secondaries = [3, 4]
    for ia, atom in enumerate(atoms):
        if atom.symbol != 'H':
            assert htypes[ia] is None
        else:
            if ia in secondaries:
                htypes[ia] == 'secondary'
            else:
                htypes[ia] == 'primary'


def test_assignment_isobutane():
    atoms = molecule('isobutane')
    htypes = hydrogen_types(atoms)

    assert htypes[1] == 'tertiary'


def test_assignment_peg():
    atoms = from_smiles('OCCN')
    htypes = hydrogen_types(atoms)

    for ia in [5, 6]:
        assert htypes[ia] == 'secondary_O'
    for ia in [7, 8]:
        assert htypes[ia] == 'secondary_N'


def test_assignment_toluene():
    atoms = from_smiles('CC1=CC=CC=C1')
    htypes = hydrogen_types(atoms)

    for ia, atom in enumerate(atoms):
        if ia in [7, 8, 9]:
            assert htypes[ia] == 'primary_sp2'
        elif atom.symbol == 'H':
            assert htypes[ia] == 'sp2'
