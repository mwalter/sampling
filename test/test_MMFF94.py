import pytest
openbabel = pytest.importorskip("openbabel")

import numpy as np
from ase.build import molecule
from ase.optimize import BFGS

from sampling.calculators.mmff94 import MMFF94
from sampling.calculators.pybel import Pybel
from sampling.optimize.internal import Internal


def test_MMFF94():
    atoms = molecule('CH2OCH2')
    atoms.calc = MMFF94()

    E0 = atoms.get_potential_energy()

    opt = BFGS(atoms)
    opt.run(fmax=0.01)
    assert E0 > atoms.get_potential_energy()


def test_pybel():
    """Test agreement to openbabel implementation"""
    atoms = molecule('CH2OCH2')
    atoms.calc = Pybel()
    Epybel = atoms.get_potential_energy()
    Fpybel = atoms.get_forces()

    atoms.calc = MMFF94()

    assert atoms.get_potential_energy() == pytest.approx(Epybel, 1e-4)
    assert atoms.get_forces() == pytest.approx(Fpybel, abs=1e-2)


def test_relaxtion():
    formula = 'CH2OCH2'
    atoms0 = molecule(formula)
    atoms0.calc = MMFF94()

    fmax = 0.05
    opt = BFGS(atoms0)
    opt.run(fmax=fmax)

    fmax = 5e-6
    atoms1 = molecule(formula)
    atoms1.calc = MMFF94()
    opt = Internal(atoms1)
    opt.run(fmax=fmax)
    assert np.abs(atoms1.get_forces()).max() < fmax

    assert atoms0.get_potential_energy() == pytest.approx(
        atoms1.get_potential_energy(), 1e-2)


def test_change_atoms():
    atoms0 = molecule('CH2OCH2')
    atoms0.calc = MMFF94()

    atoms0.get_potential_energy()
    id0 = id(atoms0.calc.mol)

    atoms0[3].position[2] += 0.1
    atoms0.get_potential_energy()
    # we changed position only, so mol should be the same
    assert id0 == id(atoms0.calc.mol)

    atoms0[3].symbol = 'F'
    atoms0.get_potential_energy()
    # we changed numbers, so mol should be different
    assert id0 != id(atoms0.calc.mol)
