from sampling.bondlength import bondlength


def test_symmetry():
    for other in 'NOS':
        assert bondlength('C', other) == bondlength(other, 'C')


def test_bondorder():
    for other in 'CNOS':
        assert bondlength('C', other, bondorder=1) \
               > bondlength('C', other, bondorder=2)
