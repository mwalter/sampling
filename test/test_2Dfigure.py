import pytest

rdkit = pytest.importorskip("rdkit")
openbabel = pytest.importorskip("openbabel")

from rdkit.Chem import AllChem, Draw
from sampling.translate import atoms_to_rdkit, from_smiles


def test_2D():
    atoms = from_smiles('[nH]1cnc2cncnc21')

    mol = atoms_to_rdkit(atoms)
    AllChem.Compute2DCoords(mol)
    Draw.MolToFile(mol, 'withH.png')

    mol = atoms_to_rdkit(atoms)
    mol = rdkit.Chem.RemoveHs(mol)
    Draw.MolToFile(mol, 'withoutH.png')
