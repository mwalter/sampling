import pytest
from sampling.translate import from_smiles
from sampling.construct import add
from sampling.bondlength import bondlength


def test_add():
    """Test basic adding of two structures"""
    nh3 = from_smiles('N')
    benzene = from_smiles('C1=CC=CC=C1')
    atoms = add(nh3, 1, benzene, 6)
    assert atoms.get_distance(0, 3) == pytest.approx(
        bondlength('C', 'N'))

    assert len(atoms) == len(nh3) + len(benzene) - 2


def test_overlapping():
    """Ensure no overlapping structures"""
    atoms = from_smiles(r'C/CC\C')
    merged = add(atoms, 4, atoms, 12, optimize=True)

    assert len(merged) == 2 * len(atoms) - 2
