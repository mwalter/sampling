import pytest
import numpy as np
from ase.build import molecule
from ase.geometry.analysis import Analysis

from sampling.construct import repeat_monomer
from sampling.translate import from_smiles


def test_polyethylene():
    atoms = molecule('C2H6')

    n = 8
    polymer = repeat_monomer(atoms, [2, 5], n)
    assert len(polymer) == 2 + n * (len(atoms) - 2)


def test_benzene_aligned():
    atoms = molecule('C6H6')

    # dimer
    polymer = repeat_monomer(atoms, [9, 11], 2)

    # check correct alignment
    long_c = polymer[11].position - polymer[2].position
    bond_c = polymer[14].position - polymer[5].position
    assert np.dot(long_c, bond_c) == pytest.approx(
        np.linalg.norm(long_c) * np.linalg.norm(bond_c)
    )

    # trimer
    polymer = repeat_monomer(atoms, [9, 11], 3)

    # check correct alignment
    long_c = polymer[13].position - polymer[21].position
    bond_c = polymer[16].position - polymer[24].position
    assert np.dot(long_c, bond_c) == pytest.approx(
        np.linalg.norm(long_c) * np.linalg.norm(bond_c)
    )


def test_overlapping():
    """Ensure no overlapping structures"""
    atoms = from_smiles(r'C/CC\C')

    polymer = repeat_monomer(atoms, [4, 12], 10, optimize=True)
    ana = Analysis(polymer)
    for bonds in ana.all_bonds[0]:
        assert len(bonds) < 5
