========
Sampling
========

Project for the exploration of sampling methods.

Install
=======

The package can be installed via pip::

  python3 -m pip install git+https://gitlab.com/ag_walter/sampling.git

Examples
========

There are calculators available in ASE style::

  from ase.build import molecule
  from sampling.mmff94 import MMFF94
  from sampling.pybel import Pybel

  atoms = molecule('CH4')

  atoms.calc = MMFF94()
  print('MMFF94 energy', atoms.get_potential_energy(), 'eV')

  from openbabel import pybel

  for ff in pybel.forcefields:
      atoms.calc = Pybel(forcefield=ff)
      print(ff.upper(), 'energy', atoms.get_potential_energy(), 'eV')

See an initial example for the creation of conformers in
doc/
