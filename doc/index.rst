========
Sampling
========

Sampling makes use of ASE_ and rdkit_.

.. _ASE: https://wiki.fysik.dtu.dk/ase
.. _rdkit: https://www.rdkit.org


.. toctree::
   :caption: Table of contents
   :maxdepth: 2
   
  documentation/calculators
  documentation/structures/add/overlapping
  documentation/structures/polymers/overlapping