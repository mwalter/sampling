===========
Calculators
===========

Sampling proves ASE_ like interfaces to some additional calculators

.. _ASE: https://wiki.fysik.dtu.dk/ase

.. literalinclude:: calculators.py

The result is pretty bad as N2 is predicted to be unbound, though.
