from ase import Atoms
from ase.build import molecule

from sampling.calculators.pybel import Pybel
from sampling.calculators.mmff94 import MMFF94

calcs = {
    'pybel implementation of UFF': Pybel(forcefield='uff'),
    'pybel implementation of MMFF94': Pybel(),
    'rdkit implementation of MMFF94': MMFF94(),}

N2 = molecule('N2')
N = Atoms('N')

print('Atomization energies of N2')
for txt, calc in calcs.items():
    N2.calc = calc
    E2 = N2.get_potential_energy()
    N.calc = calc
    E = N.get_potential_energy()
    
    print(f'{2 * E - E2:.2f} eV', txt)
