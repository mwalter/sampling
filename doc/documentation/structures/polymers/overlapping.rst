================
Create a polymer
================

We create the monomer cis-butane and view its structure

.. literalinclude:: monomer.py

We identify the indicees of the connection points as 4 and 12.
Then we can create a polýmer with 10 units (40 carbon atoms)

.. literalinclude:: polymer.py
