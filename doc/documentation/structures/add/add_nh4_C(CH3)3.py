from sampling.translate import from_smiles
from sampling.construct import add

nh4 = from_smiles('[NH4+]')
cch4_3 = from_smiles('CC(C)C')
# identify connecting H by 7 in cch4_3

# connect a cch4_3 at each H of NH4:
atoms = nh4.copy()
for ia, atom in enumerate(nh4):
    if atom.symbol == 'H':
        atoms = add(atoms, 1, cch4_3, 7)
atoms.edit()