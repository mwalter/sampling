import numpy as np
from ase.calculators.calculator import Calculator, all_changes


class Mueser2019(Calculator):
    """after doi 10.3390/lubricants7040035

    A linear chain of beads is assumed
    """
    implemented_properties = ['energy', 'energies', 'forces', 'free_energy']
    default_parameters = {
        'a0': 1.0,
        'k1': 1.0,
        'k2': 1.0
    }

    def calculate(
            self,
            atoms=None,
            properties=None,
            system_changes=all_changes):
        if properties is None:
            properties = self.implemented_properties

        a0 = self.parameters.a0
        k1 = self.parameters.k1

        pos_ac = atoms.get_positions()
        dleft_ac = np.zeros(pos_ac.shape)
        dleft_ac[1:] = pos_ac[:-1] - pos_ac[1:]
        rleft_a = np.sqrt((dleft_ac**2).sum(axis=1))
        rleft_a[0] = a0  # enable summing over all
        hleft_ac = (dleft_ac.T / rleft_a).T

        energy = k1 * ((rleft_a - a0)**2).sum() / 2
        forces_ac = k1 * ((rleft_a - a0) * hleft_ac.T).T
        forces_ac[:-1] -= k1 * ((rleft_a[1:] - a0) * hleft_ac[1:].T).T

        k2 = self.parameters.k2

        p = len(atoms) - 1
        for i in range(1, p):
            x_c = pos_ac[i] - 0.5 * pos_ac[i + 1] - 0.5 * pos_ac[i - 1]
            energy += k2 * np.dot(x_c, x_c) / 2

        self.results['energy'] = energy
        self.results['free_energy'] = energy
        self.results['forces'] = forces_ac
