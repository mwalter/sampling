import numpy as np

from ase.units import kB, J, _hplanck


def Arrhenius_rate(energy: float, omega: float, T_K: float = 300) -> float:
    """Arrhenius rate in [1/s]

    energy: activation energy [eV]
    omega: pre-exponential factor in [1/s]
    T_K: Temperature in K
    """
    beta = 1 / kB / T_K
    return np.exp(- beta * energy) * omega


def Eyring_rate(energy: float, T_K: float = 300) -> float:
    """Return the Eyring rate in [1/s]

    energy: [eV]
    T_K: Temperature [K]
    """
    beta = 1 / kB / T_K
    h = _hplanck * J  # eV s
    omega = 1 / (beta * h)
    return Arrhenius_rate(energy, omega, T_K)


def Eyring_barrier(rate: float, T_K: float = 300) -> float:
    """Return the barrier in [eV] corresponding to an Eyring rate

    rate: [1/s]
    T_K: Temperature [K]
    """
    beta = 1 / kB / T_K
    h = _hplanck * J  # eV s
    return - np.log(beta * h * rate) / beta
