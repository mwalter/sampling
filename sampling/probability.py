from typing import List, Tuple, Optional
from ase.units import kB
import numpy as np
import numpy.typing as npt


def partition_sum(energies: npt.NDArray, T_K: float) -> Tuple[float, float]:
    """Returns partition sum and reference energy"""
    energies = np.array(energies)
    Eref = energies.min()
    beta = 1 / kB / T_K

    return np.sum(np.exp(-beta * (energies - Eref))), Eref


def probabilities(energies: npt.NDArray, T_K: float,
                  Z: Optional[float] = None, Eref: float = 0) -> float:
    """
    partition sum and reference energy can be given"""
    beta = 1 / kB / T_K
    if Z is None:
        Z, Eref = partition_sum(energies, T_K)

    return np.exp(-beta * (energies - Eref)) / Z


def entropy(probabilities: List[float]) -> float:
    """Entropy from a given set of probabilities"""
    p = np.array(probabilities)
    return - kB * (p[p > 0] * np.log(p[p > 0])).sum()
