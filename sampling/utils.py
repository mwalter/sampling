import tempfile
import numpy as np
try:
    from rdkit import Chem
except ModuleNotFoundError:
    Chem = None
try:
    from openbabel import pybel
except ModuleNotFoundError:
    openbabel = None

from ase import Atoms
from ase.build.attach import random_unit_vector
from ase.parallel import world, broadcast


def next_neighbor(atoms, ia):
    """Return index of next neighbor to index ia"""
    rmin = 1e23
    if ia < 0:
        ia = len(atoms) + ia
    for ib, btom in enumerate(atoms):
        r = atoms.get_distance(ia, ib)
        if ib != ia and r < rmin:
            rmin = r
            imin = ib
    return imin


def random_perpendicular_vector(def_c, rng=np.random, comm=world):
    """Return a random vector perpendicular to a given vector

    def_c: defining vector
    rng: random number generator
    comm: mpi communicator to distribute the vector
    """
    hdef_c = np.array(def_c) / np.linalg.norm(def_c)

    norm = 0
    while norm < 0.01:
        rp_c = random_unit_vector(rng)
        rp_c = broadcast(rp_c, comm=comm)

        rp_c = np.cross(hdef_c, rp_c)
        norm = np.linalg.norm(rp_c)

    return rp_c / norm


def atoms_to_mol(atoms: Atoms) -> Chem:
    """Use openbabel to translate Atoms to Chem"""
    # hack to get bonds
    with tempfile.NamedTemporaryFile(suffix='.xyz') as tf:
        atoms.write(tf.name, format='xyz')
        mol = next(pybel.readfile('xyz', tf.name))

    return Chem.MolFromMolBlock(mol.write('mol'), removeHs=True)
