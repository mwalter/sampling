import numpy as np
from ase import Atoms, io
from ase.build.attach import attach_randomly_and_broadcast
from typing import Optional

from .conformers import ConformerBase
from .optimize.internal import internal_or_fire
from .typing import MaybeDefinedInitiliaze


class VdwConformers(ConformerBase):
    def __init__(self, atoms1: Atoms, atoms2: Atoms):
        self.atoms1 = atoms1
        self.atoms2 = atoms2

    def add(self, number: int, distance: float, rng=np.random):
        """Add a given number of conformers"""
        for _ in range(number):
            super().append(attach_randomly_and_broadcast(
                self.atoms1, self.atoms2, distance, rng=rng))

    @classmethod
    def read(cls, trajname: str):
        traj = io.Trajectory(trajname)

        # first two strcutures are the defining Atoms
        obj = cls(traj[0], traj[1])
        n = len(obj.atoms1) + len(obj.atoms2)

        # next are conformers
        for atoms in traj[2:]:
            assert len(atoms) == n
            obj.append(atoms)

        return obj

    def relax(self,
              initialize: MaybeDefinedInitiliaze = None,
              fmax: float = 0.05,
              optimizer=internal_or_fire,
              trajname: Optional[str] = None,
              ) -> None:
        defined_initialize = self.define_initialize(initialize)

        self.atoms1 = self._relax_and_save(
            self.atoms1, defined_initialize, fmax, optimizer, trajname)
        self.atoms2 = self._relax_and_save(
            self.atoms2, defined_initialize, fmax, optimizer, trajname)

        super().relax(initialize, fmax, optimizer, trajname)

    def write(self, trajname: str):
        with io.Trajectory(trajname, 'w') as traj:
            traj.write(self.atoms1)
            traj.write(self.atoms2)
            self._write_traj(traj)
