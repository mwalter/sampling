import numpy as np
from typing import Dict, List, Optional

from rdkit import Chem

from ase import Atoms, Atom
from ase.build.attach import random_unit_vector

from .translate import (atoms_to_openbabel, atoms_to_rdkit,
                        openbabel_to_rdkit, rdkit_to_atoms)
from .bond_order import (bonded_indices, bond_order, sp, nbonds, bondlengths,
                         lone_pair_direction)
from .carbon import coordination


def add_sp3(atoms: Atoms, index: int, scale: float = 1.5,
            rng=np.random) -> Atoms:
    """add hydrogens to a sp3 carbon"""
    bonded = bonded_indices(atoms, index, scale)
    nadd = nbonds[atoms[index].symbol] - len(bonded)
    bondlength = bondlengths[atoms[index].symbol + 'H'][0]
    # XXX setting the angle does not work, but 120° works approximately
    # angle = table['angle'][atoms[index].symbol] * np.pi / 180
    angle = 120 * np.pi / 180

    if nadd <= 0:
        return atoms

    if len(bonded) == 0:
        r = random_unit_vector(rng) * bondlength
        atoms.append(Atom('H', position=r))
    elif len(bonded) == 3:
        vec = lone_pair_direction(atoms, index, bonded)
    else:
        if len(bonded) > 0:
            v0 = atoms[bonded[0]].position - atoms[index].position
        if len(bonded) == 2:
            v1 = atoms[bonded[1]].position - atoms[index].position
        else:
            vperp = np.cross(v0, rng.rand(3))
            hperp = vperp / np.linalg.norm(vperp)
            hin = v0 / np.linalg.norm(v0)
            v1 = hperp * np.sin(angle) + hin * np.cos(angle)

        vperp = np.cross(v0, v1)
        hperp = vperp / np.linalg.norm(vperp)
        vin = (v0 + v1)
        hin = vin / np.linalg.norm(vin)

        vec = hperp * np.sin(angle) + hin * np.cos(angle)

    if len(bonded) != 0:
        position = atoms[index].position + bondlength * vec
        atoms.append(Atom('H', position=position))

    return add_sp3(atoms, index, scale, rng)


def add_sp2(atoms: Atoms, index: int, scale: float = 1.5,
            rng=np.random) -> Atoms:
    """add hydrogens to a sp3 carbon"""
    bonded = bonded_indices(atoms, index, scale)
    nbf: float = 0
    for ib in bonded:
        nbf += bond_order(atoms, index, ib)
    nb: int = int(nbf + 0.5)

    nadd = nbonds[atoms[index].symbol] - nb
    if nadd < 1:
        return atoms

    if len(bonded) == 1:
        # one bond
        bvec = atoms[bonded[0]].position - atoms[index].position
        bvec /= np.linalg.norm(bvec)
        pvec = np.zeros(3, float)
        while (pvec == 0).all():
            pvec = np.cross(bvec, rng.random(3))
        pvec / np.linalg.norm(pvec)
        angle = 2 * np.pi / 3
        vec = np.cos(angle) * bvec + np.sin(angle) * pvec
    elif len(bonded) == 2:
        # two bonds
        vec = np.zeros(3, float)
        for ib in bonded:
            dvec = atoms[ib].position - atoms[index].position
            vec -= dvec / np.linalg.norm(dvec)

    bondlength = bondlengths[atoms[index].symbol + 'H'][0]
    vec *= bondlength / np.linalg.norm(vec)

    atoms.append(Atom('H', position=atoms[index].position + vec))

    return add_sp2(atoms, index, scale, rng)


def add_sp1(atoms: Atoms, index: int, scale: float = 1.5,
            rng=np.random) -> Atoms:
    """add hydrogens to a sp3 carbon"""
    bonded = bonded_indices(atoms, index, scale)
    nbf: float = 0
    for ib in bonded:
        nbf += bond_order(atoms, index, ib)
    nb: int = int(nbf + 0.5)

    nadd = nbonds[atoms[index].symbol] - nb
    if nadd < 1:
        return atoms

    assert len(bonded) == 1

    # one bond
    vec = atoms[bonded[0]].position - atoms[index].position
    bondlength = bondlengths[atoms[index].symbol + 'H'][0]
    vec *= - bondlength / np.linalg.norm(vec)

    atoms.append(Atom('H', position=atoms[index].position + vec))

    return atoms


def stripH(atoms: Atoms) -> Atoms:
    result = Atoms(pbc=atoms.pbc, cell=atoms.cell)
    for atom in atoms:
        if atom.symbol != 'H':
            result.append(atom)
    return result


def addH_pybel(atoms: Atoms) -> Atoms:
    """Add hydrogens using openbabel -> does not work"""
    mol = atoms_to_openbabel(atoms)
    print('vorher', len(mol.atoms))
    mol.OBMol.AddHydrogens()
    print('nachher', len(mol.atoms))
    mol = openbabel_to_rdkit(mol)
    return rdkit_to_atoms(mol)


def neutralize_atoms(mol):
    """from https://rdkit.org/docs/Cookbook.html#neutralizing-molecules"""
    pattern = Chem.MolFromSmarts(
        "[+1!h0!$([*]~[-1,-2,-3,-4]),-1!$([*]~[+1,+2,+3,+4])]")
    at_matches = mol.GetSubstructMatches(pattern)
    at_matches_list = [y[0] for y in at_matches]
    print('#####', at_matches_list)
    if len(at_matches_list) > 0:
        for at_idx in at_matches_list:
            atom = mol.GetAtomWithIdx(at_idx)
            chg = atom.GetFormalCharge()
            hcount = atom.GetTotalNumHs()
            atom.SetFormalCharge(0)
            atom.SetNumExplicitHs(hcount - chg)
            atom.UpdatePropertyCache()
    return mol


def addH_rdkit(atoms: Atoms) -> Atoms:
    """Add hydrogens using rdkit -> does not work"""
    mol = atoms_to_rdkit(atoms)
    print(mol)
    mol2 = neutralize_atoms(mol)
    print(mol2)
    return rdkit_to_atoms(mol2)


def addH(atoms: Atoms) -> Atoms:
    """Add hydrogens depending on hybridization

    own implementation, works so-far
    """
    btoms = atoms.copy()
    for ia, atom in enumerate(btoms):
        if atom.symbol == 'H':
            continue
        assert atom.symbol in 'CNOS'
        spval = sp(atoms, ia)
        if spval == 3:
            atoms = add_sp3(atoms, ia)
        elif spval == 2:
            atoms = add_sp2(atoms, ia)
        elif spval == 1:
            atoms = add_sp1(atoms, ia)
        else:
            assert 0, f'sp{spval} not implemented'

    return atoms


def _assign_type(atoms: Atoms, coord: List[Dict], ib: int):
    if atoms[ib].symbol == 'C':
        if sp(atoms, ib) == 2:
            return 'sp2'

        # check neighbors
        sp2_neighbor = False
        if 'C' in coord[ib]:
            for ic in bonded_indices(atoms, ib):
                if sp(atoms, ic) == 2:
                    sp2_neighbor = True

        ext = ''
        if sp2_neighbor:
            ext = '_sp2'

        for elem in 'NO':
            if elem in coord[ib]:
                ext += f'_{elem}'

        if coord[ib]['H'] >= 3:
            return 'primary' + ext
        if coord[ib]['H'] == 2:
            return 'secondary' + ext
        return 'tertiary' + ext

    return atoms[ib].symbol


def hydrogen_types(atoms) -> List[Optional[str]]:
    coord = coordination(atoms)

    htypes: List[Optional[str]] = []
    for ia, atom in enumerate(atoms):
        if atom.symbol != 'H':
            htypes.append(None)
            continue

        indices = bonded_indices(atoms, ia)
        assert len(indices) == 1
        ib = indices[0]

        htypes.append(
            _assign_type(atoms, coord, ib))

    return htypes
