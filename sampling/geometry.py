def align(struct, xaxis='x', yaxis='y'):
    """Align moments of inertia with the coordinate system."""
    struct.translate(-struct.get_center_of_mass())

    Is, Vs = struct.get_moments_of_inertia(True)
    IV = list(zip(Is, Vs))
    IV.sort(key=lambda x: x[0])
    struct.rotate(IV[0][1], xaxis)

    Is, Vs = struct.get_moments_of_inertia(True)
    IV = list(zip(Is, Vs))
    IV.sort(key=lambda x: x[0])
    struct.rotate(IV[1][1], yaxis)
