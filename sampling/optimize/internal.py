from ase.optimize.optimize import Optimizer
from ase.optimize import FIRE


class Internal(Optimizer):
    def __init__(self, atoms,
                 restart=None, logfile='-', trajectory=None,
                 master=None):
        super().__init__(atoms, restart, logfile, trajectory, master)

    def run(self, fmax=0.05, steps=None):
        """call internal run and collect results"""
        self.fmax = fmax
        if steps:
            self.max_steps = steps

        self.atoms.calc.optimize(self.atoms, fmax, steps)
        self.atoms.set_positions(self.atoms.calc.atoms.get_positions(),
                                 apply_constraint=False)


def internal_or_fire(atoms, *args, **kwargs):
    if hasattr(atoms.calc, 'optimize'):
        return Internal(atoms, *args, **kwargs)
    return FIRE(atoms, *args, **kwargs)
