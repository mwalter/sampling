from typing import Optional


# PBE bondlengths, for single and double bonds
bondlengths = {
    'CC': [1.528, 1.332],  # C2H6, C2H4
    # 'CN': [1.468, 1.273],  # H3CNH2, CH3N, preblems with HCN
    'CN': [1.468, 1.313],  # H3CNH2, derived from HCN
    'CO': [1.432, 1.213],  # H3COH, CH2O
    'CS': [1.825, 1.615],  # CH4S, CH2S
    'CH': [1.096],  # CH4
    'NH': [1.022],  # H3N
    'OH': [0.976],  # H2O
    'SH': [1.352],  # H2S
}


def bondlength(symbol1: str, symbol2: str, bondorder: int = 1) \
        -> Optional[float]:
    """Return expected bondlength"""
    symbols = ''.join(sorted([symbol1, symbol2]))
    if symbols not in bondlengths:
        return None

    try:
        return bondlengths[symbols][bondorder - 1]
    except KeyError:
        return None
